from django import forms
from filmkritiken.models import Film, Review, Genre

class FilmForm(forms.ModelForm):
    class Meta:
        model = Film
        fields = [
            "title",
            "poster",
            "trailer",
            "year",
            "director",
            "plot_summary",
            "genres"
        ]

    genres = forms.ModelMultipleChoiceField(
        queryset=Genre.objects.all(),
        widget=forms.CheckboxSelectMultiple,
    )

    
class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = [
            "rating",
            "content",
        ]