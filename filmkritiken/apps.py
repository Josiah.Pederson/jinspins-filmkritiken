from django.apps import AppConfig


class FilmkritikenConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'filmkritiken'
