from django.db import models
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
USER_MODEL = settings.AUTH_USER_MODEL
# Create your models here.

class Film(models.Model):
    title = models.CharField(max_length=60)
    poster = models.URLField(null=True, blank=True)
    trailer = models.URLField(null=True, blank=True)
    year = models.IntegerField(
        validators=(
            MaxValueValidator(2022),
            MinValueValidator(1900)
        ), default=2022
    )
    director = models.CharField(max_length=30)
    plot_summary = models.TextField(max_length=500)

    def __str__(self):
        return self.title

class Review(models.Model):
    film = models.ForeignKey("Film", related_name="review", on_delete=models.CASCADE)
    reviewer = models.ForeignKey(USER_MODEL, related_name="reviewer", on_delete=models.PROTECT)
    reviewed_on = models.DateField(auto_now_add=True)
    rating = models.IntegerField(
        validators=(
            MinValueValidator(1),
            MaxValueValidator(10)
        ), default=5
    )
    content = models.TextField()

    def __str__(self):
        return f"{self.film} reviewed by {self.reviewer}"


class Genre(models.Model):
    films = models.ManyToManyField("Film", related_name="genres", blank=True)
    name = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.name}"
