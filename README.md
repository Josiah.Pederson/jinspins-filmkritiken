# Filmkritiken

## Description
Filmkritiken is a film review website where enthusiasts can create watchlists and share their reviews of movies they have seen.

## Wireframes

![Homepage](wireframes/homepage.png)
![Movie detial page](wireframes/detail.png)
![Watchlist](wireframes/watchlist.png)

## Tech stack
* Django
* Python
* HTML
* CSS
* SQLite

## Getting started
* Fork and clone this repo
* In the root directory, create and start a virtual environment
* Install the requirements.txt into the virtual environment
* Run "python manage.py migrate" to set up SQLite database
* Run "python manage.py createsuperuser" to create admin account
* Start the server with "python manage.py runserver"
* Head to localhost:8000/films to login or create an account and get started!

## Roadmap
Future features include:
* Shared watchlists: Friends can contribute to a shared watchlist
* Watch parties: When friends can't decide on a movie, the watchlist feature will compare their respective watchlists and desired genre(s) and provide suggestions.
