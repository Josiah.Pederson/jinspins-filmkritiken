from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.
class WatchlistItem(models.Model):
    owner = models.ForeignKey(USER_MODEL, related_name="watchlist_item", on_delete=models.CASCADE)
    film = models.ForeignKey("filmkritiken.Film", related_name="watchlists", on_delete=models.PROTECT)
    watched = models.BooleanField(verbose_name="Watched", default=False, blank=False)

    def __str__(self):
        return f"{self.owner}: {self.film}"
