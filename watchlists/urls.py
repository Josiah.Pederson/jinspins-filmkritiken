from django.urls import path
from watchlists.views import(
    watchlist,
)
from filmkritiken.views import(
    new_watchlist_item,
)

urlpatterns = [
    path("", watchlist, name="watchlist_url"),
    path("<int:pk>/add/", new_watchlist_item, name="new_watchlist_item_url")
]
