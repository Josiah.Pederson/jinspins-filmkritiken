from watchlists.models import WatchlistItem
from django.shortcuts import redirect, render
from django.core.paginator import Paginator

# Create your views here.

def watchlist(request):
    query = request.GET.get("q")
    if not query:
        query = ""
    films = WatchlistItem.objects.filter(film__title__icontains=query)
    paginator = Paginator(films, 20)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        "page_obj": page_obj,
        "total_watchlist_items": len(films)
    }
    return render(request, "watchlists/list.html", context)
